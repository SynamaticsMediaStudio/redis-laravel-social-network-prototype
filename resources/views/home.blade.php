@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @redisauth
            <div class="card mb-3">
                <div class="card-header">
                    <label for="post">Share Your Post</label>

                </div>
                <form action="{{route('posts.store')}}" method="POST" class="form">
                    @csrf
                    <textarea name="post" id="post" required rows="4" class="form-control"></textarea>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Share</button>
                    </div>
                </form>
            </div>
            @endredisauth
            @for ($i = 0; $i < $posts->count(); $i++)
            <div class="card mb-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <i class="far fa-1x fa-user-circle"></i>
                            <strong>{!!$posts[$i]['user']['name']!!}</strong><br/>
                            <small class="text-black-50">{{Carbon::parse($posts[$i]['created_at'])->diffForHumans()}}
                                @if($posts[$i]['created_at'] !== $posts[$i]['updated_at'])
                                ;Updated:
                                {{Carbon::parse($posts[$i]['updated_at'])->diffForHumans()}}
                                @endif
                            </small>
                        </div>
                    </div>
                    <div class="py-2">
                        {{$posts[$i]['content']}}
                    </div>
                </div>
                <div class="card-footer py-1">
                    <a class=""><i class="fas fa-heart"></i></a> {{$posts[$i]['likes']}}
                    @redisauth
                    @if(AppAuth::auth()->email == $posts[$i]['user']['email'])
                    <a data-toggle="modal" data-target="#edit-post-modal" data-post-id="{{$i}}" data-post="{{$posts[$i]['content']}}" href="">Edit</a>
                    @endif
                    @endredisauth
                </div>
            </div>
                
            @endfor
            {{$posts->links()}}
        </div>
    </div>
<div class="modal fade" id="edit-post-modal" tabindex="-1" role="dialog" aria-labelledby="edit-post-modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form id="editpostform" method="POST" action="">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit-post-modalLabel">Edit Post</h5>
      </div>
      <div class="modal-body">
            @csrf
            <input type="hidden" name="_method" value="PUT">
          <div class="form-group">
            <label for="post_edit" class="col-form-label">Post</label>
            <textarea name="post" id="post_edit" required rows="4" class="form-control"></textarea>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </div>
    </form>
  </div>
</div>    
</div>
<script>
$(function(){
    $('#edit-post-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var id = button.data('post-id')
    var post = button.data('post')
    var modal = $(this)
    modal.find('#editpostform').attr('action',`/posts/${id}`);
    modal.find("#post_edit").val(post)
    })    
})
</script>
@endsection
