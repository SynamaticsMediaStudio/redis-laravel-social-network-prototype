<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('auth')->group(function () {
    Route::post('/login', 'RedisAuthController@login')->name('redis.login');
    Route::post('/register', 'RedisAuthController@register')->name('redis.register');
    Route::post('/logout', 'RedisAuthController@logout')->name('redis.logout');
});
Route::get('/', 'HomeController@index')->name('home');
Route::resource('/posts', 'PostsController');