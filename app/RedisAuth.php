<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class RedisAuth extends Model
{

    public  $name,
            $email,
            $password,
            $email_verified,
            $isLoggedIn=false,
            $user=null,
            $errors = [];

    public function __construct(){
     if(session('user_login')){
         $this->isLoggedIn  = true;
         $this->user        = Redis::get('user:'.decrypt(session('user_login')));
     }
    }
    
    public function auth()
    {
        return json_decode($this->user);
    }
    
    public function login($email,$password)
    {
        $this->email = $email;
        $this->password = $password;
        $hasher = app('hash');
        $this->user = Redis::get('user:'.$email);
        if($this->user){
            $check = $hasher->check($this->password, $this->auth()->password);
            if($check){
                session(['user_login' => encrypt($this->email)]);
                $this->isLoggedIn = true;
            }
            else{
                $this->errors = ['password'=>['Incorrect Password']];
            }
        }
        else{
            $this->errors = ['email'=>['Email not Found']];
        }
        return $this;
    }
    public function logout()
    {
        session()->forget('user_login');
        return true;
    }
    public function register()
    {
        $hasher = app('hash');
        if($this->email && $this->password && $this->name){
            $user = Redis::get('user:'.$this->email);
            if($user){
                $this->errors = ['email'=>['Email already Registered']];
                return false;
            }
            else{
                $this->password = $hasher->make($this->password);
                Redis::set('user:'.$this->email, json_encode(['name'=>$this->name,'password'=>$this->password,'email'=>$this->email]));
                session(['user_login' => encrypt($this->email)]);
                return true;
            }
                        
        }
        else{
            return false;
        }
    }
}
